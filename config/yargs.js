const opts = {
    base: {
        demand: true,
        alias: 'b'
    },
    limite: {
        alias: 'l',
        default: 10
    }
}


const argv = require('yargs')
    // Para poner el comando como listar ejecutando en pantalla: node app listar -b 5 -l 20
    .command('listar', 'Imprime en consola la tabla de multiplicar', opts)
    .command('crear', 'Genera un archivo la tabla de multiplicar', opts)
    // Para poder hacer el: node app listar --help
    .help()
    .argv;

module.exports = {
    argv
}
