const fs = require('fs');
var colors = require('colors');

// también se puede añadir a los exports directamente así:
// module.exports.crearArchivo = ....

let crearArchivo = (base, limite = 10) => {
    return new Promise((resolve, reject) => {
        if (!Number(base)) {
            reject(`El valor introducido: ${base} no es un número`);
            return;
        }
        let data = '';

        for (let i = 0; i <= limite; i++) {
            data += `${base} * ${i} = ${base * i}\n`;
        }

        fs.writeFile(`./tablas/tabla-${base}-hasta-${limite}.txt`, data, (err) => {
            if (err) {
                reject(err);
                return;
            };
            resolve(`tabla-${base}-hasta-${limite}.txt`)
            // console.log(`El archivo tabla-${base} ha sido creado`);
        });

    });
}

let listarTabla = (base, limite = 10) => {
    console.log('========================'.red);
    console.log(`===== Tabla de ${base}=======`.green);
    console.log('========================'.red);
    for (let i = 0; i <= limite; i++) {
        console.log(`${base} * ${i} = ${base * i}`);
    }
}

module.exports = {
    // crearArchivo: crearArchivo
    crearArchivo,
    listarTabla
};


