// requireds
// const fs = require('fs');
// const fs = require('express');
// const fs = require('fs');

// Se puede hacer así para traer la función crearArchivo, pero se suele usar la desestructuración
// const multiplicar = require('./multiplicar/multiplicar');
// multiplicar.crearArchivo

// existen variables de entorno en node, una es el module, que viaja por toda la aplicación 
// y tiene las exportaciones, entre otras cosas
// otra variable de entorno en node es el process, que tiene datos del sistema operativo, el usuario
// que lo corre, etc... También tiene el argv, que son los argumentos a la hora de llamar al programa
// el argv tiene como mínimo la localización del node y de segundo argumento la ubicación del archivo

const argv = require('./config/yargs').argv;
const { crearArchivo, listarTabla } = require('./multiplicar/multiplicar');
var colors = require('colors/safe');


let comando = argv._[0];
switch (comando) {
    case 'listar':
        listarTabla(argv.base, argv.limite);
        break;
    case 'crear':
        crearArchivo(argv.base, argv.limite)
            .then(archivo => console.log(`Archivo creado:`, colors.green(archivo)))
            .catch(e => console.log(e));
        break;

    default:
        console.log('Comando no reconocido');
        break;
}



// let base = '4';

// console.log(process.argv);
// Recuperando la base con el process...
// let argv = process.argv;
// let parametro = argv[2];
// let base = parametro.split('=')[1];

// Recuperando la base con yargs

